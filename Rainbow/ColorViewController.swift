//
//  ColorViewController.swift
//  Rainbow
//
//  Created by Spencer Sallay on 9/20/15.
//  Copyright © 2015 Spencer Sallay. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {
    
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var colorView: UIView!
    var color : Color = Color(name:"",rgba:UIColor(red:0,green:0,blue:0,alpha:0))

    override func viewDidLoad() {
        super.viewDidLoad()
        print(color.name)
        colorView.backgroundColor = color.rgba
        colorLabel.text = color.name
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
