//
//  RbTableViewController.swift
//  Rainbow
//
//  Created by Spencer Sallay on 9/19/15.
//  Copyright © 2015 Spencer Sallay. All rights reserved.
//

import UIKit

struct Color {
    var name : String = ""
    var rgba : UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
}

class RbTableViewController: UITableViewController {

    let colors : [Color] = [
        Color(name:"Violet", rgba:UIColor(red: 0.624, green: 0, blue: 1.0, alpha: 1.0)),
        Color(name:"Blue", rgba:UIColor(red: 0, green: 0, blue: 1.0, alpha: 1.0)),
        Color(name:"Green", rgba:UIColor(red: 0.11, green: 0.675, blue: 0.471, alpha: 1.0)),
        Color(name:"Yellow", rgba:UIColor(red: 1.0, green: 1.0, blue: 0, alpha: 1.0)),
        Color(name:"Orange", rgba:UIColor(red: 1.0, green: 0.498, blue: 0, alpha: 1.0)),
        Color(name:"Red", rgba:UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0)),
        Color(name:"Indigo", rgba:UIColor(red: 0.294, green: 0, blue: 0.509, alpha: 1.0))]
    
    var currentColor : Color!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentColor = colors[0]

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return colors.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LabelCell", forIndexPath: indexPath) as! RbTableViewCell

        // Configure the cell...
        
        cell.titleLabel.text = colors[indexPath.row].name
        cell.colorBox.backgroundColor = colors[indexPath.row].rgba
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        currentColor = colors[indexPath.row]
        print("2:\(currentColor.name)")
        performSegueWithIdentifier("CView", sender:indexPath)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        if segue.identifier == "CView" {
          //let cc = sender as! NSIndexPath
          let newView = segue.destinationViewController as! ColorViewController
          // Pass the selected object to the new view controller.
            print("1:\(currentColor.name)")
            newView.color = Color(name:currentColor.name,rgba:currentColor.rgba)
        }
        
    }
    

}
